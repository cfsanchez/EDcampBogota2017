<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreChapter;
use App\Models\Chapter;
use App\Models\Historical;
use App\Models\Serie;
use App\Models\Subscription;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ChaptersController extends Controller
{
    public function create(Serie $serie)
    {
        return view('chapters.create', compact('serie'));
    }

    public function store(StoreChapter $request)
    {
        $serie_id = $request->route()->parameter('serie');

        $chapter = new Chapter($request->only(['name', 'description', 'chapter_url']));

        $duration = preg_split('/:/', $request->get('duration'));
        $chapter->duration = Carbon::createFromTime($duration[0], $duration[1], $duration[2]);

        $chapter->serie_id = $serie_id;
        $chapter->save();

        return redirect()->route('admin.chapters.show', [$chapter]);
    }

    public function show($id)
    {
        $subscription = Subscription::where('user_id', auth()->user()->id)
            ->where('ends_at', '>', Carbon::now())
            ->first();

        if (!$subscription && auth()->user()->role_id !== 1) {
            return redirect()->route('subscriptions.subscribe');
        }

        $chapter = Chapter::findOrFail($id);

        $historical = Historical::where('chapter_id', $chapter->id)
            ->where('user_id', auth()->user()->id)
            ->first();

        if (!$historical) {
            $historical = new Historical();
            $historical->chapter_id = $chapter->id;
            $historical->user_id = auth()->user()->id;
        }

        $historical->updated_at = Carbon::now();
        $historical->save();

        return view('chapters.show', compact('chapter'));
    }

    public function edit($id)
    {
        $chapter = Chapter::findOrFail($id);

        return view('chapters.edit', compact('chapter'));
    }

    public function update(StoreChapter $request, $id)
    {
        $chapter = Chapter::findOrFail($id);
        $chapter->name = $request->get('name');
        $chapter->chapter_url = $request->get('chapter_url');
        $chapter->description = $request->get('description');

        $duration = preg_split('/:/', $request->get('duration'));
        $chapter->duration = Carbon::createFromTime($duration[0], $duration[1], $duration[2]);

        $chapter->save();

        return redirect()->route('admin.chapters.show', [$chapter]);
    }
}
